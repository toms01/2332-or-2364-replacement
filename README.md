# 2332 or 2364 replacement

A replacement for the 2332 and 2364 ROMs used in Commodore computers like VC20, C64, PET
with configurable chip-select logic via solder bridges.

The programming of the 28C64 can be done using the appropriate programming adapter
![2332/64](/Pictures/2332-64.jpeg "2332-64 and the programming adapter")

The configuration is set via the solder bridges on the bottom side (=> 2332-64-Configuration.pdf) 
![2332/64](/Pictures/2332-64-2.jpeg "2332-64")

To fill up the 8kb for the 28C64 in 2332 mode just double the content

![C64](/Pictures/2332-64-C64.jpeg "C64")
![VC20](/Pictures/2332-64-VC20.jpeg "VC20")
![P500](/Pictures/2332-64-P500.jpeg "P500")
